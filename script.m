mri = imread('256_MRI_z_109.png');
z = zeros(256*256,1);
x = zeros(256*256,1);
y = zeros(256*256,1);
c = zeros(256*256,3);
for a=1:256
    for b=1:256
        if(mri(a,b) > 0)
            y((a*256)+b) = b;
            x((a*256)+b) = a;
            z((a*256)+b) = mri(a,b);
            if(mri(a,b) < 85)
                c((a*256)+b,:) = [1 0 0];
            elseif(mri(a,b) < 170)
                c((a*256)+b,:) = [0 1 0];
            else
                c((a*256)+b,:) = [0 0 1];
            end
        end
    end
end
%scatter3(x,y,z,1,c);
surf(x,y,z);
%imshow(mri);