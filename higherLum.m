clear;
clc;
%I = imread("256_MRI_z_109.png");
I = rgb2gray(imread("data/3635222transverse0_76.gif"));
[m n] = size(I);
K = 256 * ones(m,n);
H = K - I;
J = H;
[s, i] = sort(H);

%ordina i pixel in base alla luminosita per colonna e li ritorna nella matrice s
% in i inserisce gli indici di riga nella matrice originale

for x=1:m
	for y=1:n
		if(s(x,y) > 130 && (s(x,y) < 170))
			H(i(x,y),y) = 0;
		else
			H(i(x,y),y) = 255;
		end
	end
end
image
subplot(2,2,1),imshow(I),title('orig');
subplot(2,2,2),imshow(J),title('mod');
subplot(2,2,3),imshow(H),title('bw');