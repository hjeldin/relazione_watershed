clear;
clc;
%I = imread('256_MRI_z_109.png');
I = imread('Gradient_of_MRI_heart_image.png');
se = strel('disk',30);
J = imtophat(I,se);
I2 = imadjust(J,[.1 .1 .1; .8 .9 1],[]);
%I2 = imtophat(I,strel('pair',[20,10]));
level = graythresh(I2);
BW = im2bw(I2,level);
D = -bwdist(~BW);
D(~BW) = -Inf;
L = watershed(D);
image
subplot(1,3,1),imshow(I),title('orig');
subplot(1,3,2),imshow(J),title('mod');
subplot(1,3,3),imshow(L),title('bw');
