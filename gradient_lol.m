clear;
clc;
I = imread ("256_MRI_z_109.png");
[m n] = size(I)
K = 256 * ones(m,n);
H = K - I;
imwrite(H,"256_white.png");
M = H(50:205,50:205);
[x y] = size(M);
N = surf(1:156,1:156,M);
view (-45,45);
shading flat;
print -dpng "256_3d.png"
%reg = regiongrowing(M);